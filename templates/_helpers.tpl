{{- define "config.loop" -}}
{{- $files := . | first }}
{{- $indentation := . | last }}
  {{- range $index, $file := $files }}
    {{- if kindIs "string" $file }}
{{ $index | indent $indentation }}:
{{ $file | indent ($indentation | add 2 | int) }}
    {{- else if kindIs "map" $file }}
{{- $loopData := list $file ($indentation | add 2 | int) }}
{{ $index | indent $indentation }}:
{{- include "config.loop" $loopData }}
    {{- end }}
  {{- end }}
{{- end -}}

{{- define "config.loopPlain" -}}
{{- $files := . | first }}
{{- $indentation := . | last }}
  {{- range $index, $file := $files }}
    {{- if kindIs "string" $file }}
{{ $index | indent $indentation }}:
{{ $file | toYaml | indent ($indentation | add 2 | int) }}
    {{- else if kindIs "map" $file }}
{{- $loopData := list $file ($indentation | add 2 | int) }}
{{ $index | indent $indentation }}:
{{- include "config.loopPlain" $loopData }}
    {{- end }}
  {{- end }}
{{- end -}}